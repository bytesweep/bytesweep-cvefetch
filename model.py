#!/usr/bin/env python
import psycopg2
import json
import datetime
from ruamel.yaml import YAML
from psycopg2.extensions import AsIs

def dbconnect():
	config_file = open('/etc/bytesweep/config.yaml','r')
	yaml=YAML()
	config = yaml.load(config_file.read())
	config_file.close()
	try:
		conn = psycopg2.connect(host=config['dbhost'],dbname=config['dbname'], user=config['dbuser'], password=config['dbpass'])
		return conn
	except:
		print('ERROR: unable to connect to the database')
		return False

def dbclose(conn):
	conn.close()

def init_tables():
	c = init_cve_table()
	md = init_cve_metadata_table()
	return c and md

def init_cve_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='cve')")
	if not cur.fetchone()[0]:
		cur.execute("create table cve (cve_id serial NOT NULL primary key,data jsonb)")
		cur.execute("CREATE INDEX idxgin ON cve USING gin (data);")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_cve_metadata_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='cve_metadata')")
	if not cur.fetchone()[0]:
		cur.execute("create table cve_metadata (metadata_id serial NOT NULL primary key,last_modified_date timestamp, last_modified_date_text text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

#INSERT
def bulk_put_cve(cvedata_list):
	conn = dbconnect()
	if not conn:
		return False
	# implement a check if the cve is already in the database
	#if get_job_by_name(job_name):
	#	return False
	cur = conn.cursor()
	for cvedata in cvedata_list:
		cur.execute("insert into cve (data) values (%s)", (json.dumps(cvedata),))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#INSERT
def put_cve(cvedata):
	conn = dbconnect()
	if not conn:
		return False
	# implement a check if the cve is already in the database
	#if get_job_by_name(job_name):
	#	return False
	cur = conn.cursor()
	cur.execute("insert into cve (data) values (%s) returning cve_id", (json.dumps(cvedata),))
	cve_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return cve_id

#INSERT
def put_metadata(last_modified_date_text):
	# convert date to postgres timestamp
	last_modified_date = last_modified_date_text
	if ":" == last_modified_date_text[-3:-2]:
		last_modified_date = last_modified_date[:-3]+last_modified_date[-2:]
	last_modified_date = datetime.datetime.strptime(last_modified_date, '%Y-%m-%dT%H:%M:%S%z')
	last_modified_date = last_modified_date.strftime("%Y-%m-%d %H:%M:00")

	conn = dbconnect()
	if not conn:
		return False
	# implement a check if the cve is already in the database
	#if get_job_by_name(job_name):
	#	return False
	cur = conn.cursor()
	cur.execute("insert into cve_metadata (last_modified_date,last_modified_date_text) values (%s,%s) returning metadata_id", (last_modified_date,last_modified_date_text))
	metadata_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return metadata_id

#SELECT
def get_cve(cve_idnum):
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in cve_idnum or '\'' in cve_idnum:
		return false
	cur = conn.cursor()
	cur.execute("select data from cve where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';",(AsIs(cve_idnum),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#SELECT
def get_latest_cve_metadata():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select last_modified_date_text from cve_metadata order by last_modified_date DESC limit 1;")
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#SELECT
def get_product_cves(product_name):
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in product_name or '\'' in product_name:
		return false
	cur = conn.cursor()
	cur.execute("select data from cve where data @> '{\"cve\": {\"affects\":{\"vendor\":{\"vendor_data\":[{\"product\":{\"product_data\":[{\"product_name\":\"%s\"}]}}]}}}}'",(AsIs(product_name),))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		prod_cves = []
		for result in results:
			prod_cves.append(result[0])
		return prod_cves
	else:
		return False

#SELECT
def get_cve_count():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select count(data) from cve")
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#DELETE DATA
def delete_cve(cve_idnum):
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in cve_idnum or '\'' in cve_idnum:
		return false
	cur = conn.cursor()
	cur.execute("delete from cve where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';",(AsIs(cve_idnum),))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#DROP
def drop_tables():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TABLE cve")
	cur.execute("DROP TABLE cve_metadata")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#UPDATE
def update_cve(cve_idnum,cvedata):
	#TODO replace with valid check
	#if job_status not in valid_job_status:
	#	return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("UPDATE cve SET data=%s where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';", (json.dumps(cvedata), AsIs(cve_idnum)))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#UPDATE
def bulk_update_cve(cvedata_list):
	#TODO replace with valid check
	#if job_status not in valid_job_status:
	#	return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	for cve_item in cvedata_list['CVE_Items']:
		cve_idnum = cve_item['cve']['CVE_data_meta']['ID']
		# security check for cve_idnum
		if '"' in cve_idnum or '\'' in cve_idnum:
			return false
		cur.execute("UPDATE cve SET data=%s where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';", (json.dumps(cve_item), AsIs(cve_idnum)))
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

