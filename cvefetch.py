#!/usr/bin/env python3
import requests
import time
import gzip
import json
import datetime
from model import put_cve, get_cve, update_cve, get_product_cves, get_latest_cve_metadata, bulk_update_cve, bulk_put_cve, put_metadata, get_cve_count

def get_cve_data(year):
	#metaurl = 'https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-'+year+'.meta'
	gzurl = 'https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-'+str(year)+'.json.gz'
	filename = '/tmp/nvdcve-1.0-'+str(year)+'.json.gz'
	try:
		r = requests.get(gzurl, stream=True)
		f = open(filename, 'wb')
		for chunk in r.iter_content(chunk_size=512):
			if chunk:
				f.write(chunk)
		f.close()
	
		f = gzip.open(filename, 'rb')
		data = f.read()
		f.close()
		return json.loads(data)
	except:
		return False

def get_modified_metadata():
	modmetaurl = 'https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-modified.meta'
	try:
		r = requests.get(modmetaurl, stream=True)
		lastModifiedDate = ''
		sha256 = ''
		for s in r.text.split('\r\n'):
			if len(s.split(':')) < 2:
				continue
			key = s.split(':')[0]
			value = ':'.join(s.split(':')[1:])
			if key == 'lastModifiedDate':
				lastModifiedDate = value
			if key == 'sha256':
				sha256 = value
	except:
		return False
	return {'lastModifiedDate':lastModifiedDate,'sha256':sha256}

def get_modified_data():
	modurl = 'https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-modified.json.gz'
	filename = '/tmp/nvdcve-1.0-modified.json.gz'
	#try:
	headers = {'User-Agent':'Wget/1.13.4 (linux-gnu)'}
	r = requests.get(modurl, stream=True, headers=headers)
	f = open(filename, 'wb')
	for chunk in r.iter_content(chunk_size=512):
		if chunk:
			f.write(chunk)
	f.close()
	
	f = gzip.open(filename, 'rb')
	data = f.read()
	f.close()
	return json.loads(data)
	#except:
	#	return False

def initial_bulk_sync():
	print("Performing Initial Bulk Sync")
	now = datetime.datetime.now()
	current_year = now.year
	for year in range(2002, current_year+1):
		cvedata = get_cve_data(year)
		bulk_put_cve(cvedata['CVE_Items'])

def modified_sync():
	mod_metadata = get_modified_metadata()
	if not mod_metadata:
		return
	current_modified_date = mod_metadata['lastModifiedDate']
	last_modified_date = get_latest_cve_metadata()

	sync_required = False
	if not last_modified_date:
		sync_required = True
	elif current_modified_date != last_modified_date:
		sync_required = True
	
	if sync_required:
		print("Performing Modified Sync")
		# perform sync
		moddata = get_modified_data()
		bulk_update_cve(moddata)
		# update metadata
		put_metadata(current_modified_date)
		
if __name__ == "__main__":
	while(True):
		# TODO: in case of stale data, need to redo bulk sync
		if get_cve_count() == 0:
			initial_bulk_sync()
		else:
			modified_sync()
		time.sleep(300)
